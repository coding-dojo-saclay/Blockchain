The idea for this coding dojo project is to understand how a blockchain works by implementing one ourselves.

We intend to proceed by following the steps described in:

http://adilmoujahid.com/posts/2018/03/intro-blockchain-bitcoin-python/

and maybe confront it with the procedure in:

https://hackernoon.com/learn-blockchains-by-building-one-117428612f46




## 22nd of March 2018

    * Reflexion of how we would proceed to deal with the new topic.
    * Quick introduction to Flask by Christian Poli
    * Creation of a VM on Gulliver. It failed during the session due to a technicality now solved; the VM was created by 
    Sébastien Gilles and is named CodingDojo-blockchain. Access will be granted to each participant of the Dojo sessions.


## April 2018

    * Clone the project described in http://adilmoujahid.com/posts/2018/03/intro-blockchain-bitcoin-python/ and play with it.
    * Discussion about the way blockwain works and behaves
    * We found out the wallet management was not considered in both links provided.

    
## 17th of May 2018

    * Reading the code 'blockchain.py'; we're in submit_transaction(). This seems shaky as it is used for both a normal transaction and the reward for mining.
    * Discussion to understand how the reward mechanism works
    * Christian gave us a tutorial link to bitcoin: https://www.youtube.com/watch?v=du34gPopY5Y
